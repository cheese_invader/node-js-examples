var http = require('http')
var PORT = 2727;

var server = http.createServer(function(req, res) {
    res.writeHead(200, {'content-type': 'text/plain', 'charset': 'utf-8'})

    if (req.url == '/summer') {
        res.write('Hot')
        res.end()
        return
    }

    if (req.url == '/winter') {
        res.write('Cold')
        res.end()
        return
    }

    res.write('Who are you?')
    res.end()
})
server.listen(PORT)

console.log(`Server listens on port ${PORT}`)
