const Router = require('koa-router')
const database = require('../../Services/DB/user')

const router = new Router()


// API Realization
const getAllUsers = async (ctx) => {
    const data = await database.fetchAll()
    ctx.body = data
}

const getUserByID = async (ctx) => {
    const user = await database.fetchByID(ctx.params.id)

    if (user) {
        ctx.body = user
    }
}

// API Declaration
router.get('/', getAllUsers)
router.get('/:id', getUserByID)


module.exports = router
