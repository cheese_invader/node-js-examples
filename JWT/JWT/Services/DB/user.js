const users = [
    {
        id: "1",
        name: "Marty"
    }, {
        id: "2",
        name: "Bob"
    }
]

// id should be String
exports.fetchByID = (id) => {
    var user = users.find(function(user) {
        return user.id === id
    })
    return user
}

exports.fetchAll = () => {
    return users
}
