const rc = require('rc')

module.exports = rc('JWT', {
    port: 2727,
    secret: 'MyTopSecretSecretButItIsNotSecretAtAllButShouldBeSecretWhyIsItNotSecret?Hmm...NowItIsSecretBecauseItIsTopSecretSecret'
})
