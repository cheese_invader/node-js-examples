const Koa = require('koa')
const Router = require('koa-router')

const config = require('./config')

const usersModule = require('./Modules/Users/users')

function createServer() {
    const server = new Koa()
    const router = new Router()

    router.get('/', ctx => {
        ctx.body = 'ok'
    })

    router.use('/users', usersModule.routes())

    server.use(router.allowedMethods())
    server.use(router.routes())

    return server
}

if (!module.parent) {
    createServer().listen(config.port, () => console.log(`Server has started\nPort: ${config.port}`))
}

module.exports = createServer
