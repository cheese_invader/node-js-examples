const { test } = require('ava')
const agent = require('supertest-koa-agent')
const createServer = require('../JWT/server')

const server = agent(createServer())

test.todo('User can successfully login')
test.todo('User gets 403 on invalid credentials')
test.todo('User receives 401 on expired token')
test.todo('User can get new tokens using refresh token')
test.todo('User can use refresh token only once')
test.todo('Refresh token become invalid on logout')
test.todo('Multiple refresh tokens are valid')
