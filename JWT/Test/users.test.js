const { test } = require('ava')
const agent = require('supertest-koa-agent')
const createServer = require('../JWT/server')

const server = agent(createServer())

test('Can get users list', async t => {
  const res = await server.get('/users')
  t.is(res.status, 200);
  t.truthy(Array.isArray(res.body))
});

test('Can get user by id', async t => {
  const res = await server.get('/users/1')
  t.is(res.body.id, '1')
  t.is(res.status, 200)
});

test('Can\'t get user by invalid id', async t => {
  const res = await server.get('/users/-1')
  t.is(res.status, 404)
});
