var database = require('../Model/database')


function isNumeric(n){
  return (typeof n == "number" && !isNaN(n));
}

exports.getAllArtists = (req, res) => {
    res.send(database.fetchAllArtists())
}


// Parameter: 'id'
exports.getArtistByID = function(req, res) {
    var id = Number(req.params.id)

    if (!isNumeric(id)) {
        res.status(400).send('id should be number')
        return
    }

    var artist = database.getArtistByID(id)

    if (artist === undefined) {
        res.status(400).send('id out of range')
        return
    }
    res.send(artist)
}

exports.addNewArtist = function(req, res) {
    var name = req.body.name
    if (name === undefined) {
        res.status(400).send('can not find name')
        return
    }

    database.addNewArtist(name)
    res.sendStatus(200)
}

// Parameter: 'id'
exports.updateArtist = function(req, res) {
    var id = Number(req.params.id)
    var name = req.body.name

    if (!isNumeric(id)) {
        res.status(400).send('id should be number')
        return
    }

    if (name === undefined) {
        res.status(400).send('can not find name')
        return
    }

    var artist = database.getArtistByID(id)

    if (artist === undefined) {
        res.status(400).send('id out of range')
        return
    }
    artist.name = name

    res.sendStatus(200)
}

exports.deleteArtist = function(req, res) {
    var id = Number(req.params.id)

    if (!isNumeric(id)) {
        res.status(400).send('id should be number')
        return
    }

    database.deleteArtistByID(id)
    res.sendStatus(200)
}
