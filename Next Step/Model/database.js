var artists = [
    {
        id: 1,
        name: 'James Hetfield'
    },
    {
        id: 2,
        name: 'Kirk Hammet'
    },
    {
        id: 3,
        name: 'Lars Ulrich'
    }
]

var currentID = 4

exports.fetchAllArtists = function() {
    return artists
}

// id: number. Returns 'null' when out of range
exports.getArtistByID = function(id) {
    var artist = artists.find(function(artist) {
        return artist.id === id
    })
    return artist
}

exports.addNewArtist = function(name) {
    var newOne = {
        id: currentID,
        name: name
    }
    currentID += 1
    artists.push(newOne)
}

// ignore when out of range
exports.deleteArtistByID = function(id) {
    artists = artists.filter(function (artist) {
        return artist.id !== id
    })
}
