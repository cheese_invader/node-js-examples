// Constants
var PORT    = 2727

// Setup server
var bodyParser = require('body-parser')
var express    = require('express')
var app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// Controllers
var artistsController = require('./Controller/artists')
var    viewController = require('./Controller/view')




// ============ API ============

// view
app.get('/', viewController.renderHomePage)

// artists
app.get('/artists',       artistsController.getAllArtists)
app.get('/artist/:id',    artistsController.getArtistByID)
app.post('/artists',      artistsController.addNewArtist)
app.put('/artist/:id',    artistsController.updateArtist)
app.delete('/artist/:id', artistsController.deleteArtist)

// ========== END API ==========




// Start server
app.listen(PORT, function() {
    console.log(`Server listens on port ${PORT}`);
})
