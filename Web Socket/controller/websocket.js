let settings = require('../settings')

let ws = require('ws')
let server = new ws.Server({ port: settings.env.WEB_SOCKET_PORT })


server.on('connection', ws => {
    ws.on('message', messageHandler)
})


// Helpfull stuff

let messageHandler = data => {
    var message = JSON.parse(data)

    if (message.message === 'exit') {
        ws.close()
        return
    }

    broadcast(message.message)
}


let broadcast = message => {
    server.clients.forEach(client => {
        if (client.readyState === ws.OPEN) {
            client.send(message)
        }
    })
}
