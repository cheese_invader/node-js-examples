const status = document.getElementById('status');
const messages = document.getElementById('messages');
const form = document.getElementById('form');
const input = document.getElementById('input');

let ws = new WebSocket('ws://localhost:2705');

function setStatus(value) {
    status.innerHTML = value;
}

function printMessage(value) {
    const li = document.createElement('li');

    li.innerHTML = value;
    messages.appendChild(li);
}

form.addEventListener('submit', event => {
    event.preventDefault();
    var message = {
        id: 1,
        name: "Marty",
        message: input.value
    }
    ws.send(JSON.stringify(message));
    input.value = '';
});

ws.onopen = () => setStatus('ONLINE');
ws.onclose = () => setStatus('DISCONNECTED');

ws.onmessage = (response) => printMessage(response.data);
