let settings = require('./settings')

let express    = require('express')
let bodyParser = require('body-parser')

let websocketController = require('./controller/websocket')
let      viewController = require('./controller/view')

let app = express()
app.use(express.static('./public'))

app.get('/', viewController.getHomePage)

app.listen(settings.env.APP_PORT, function() {
    console.log(`Server listens ${settings.env.IP_ADDERSS}:${settings.env.APP_PORT}`);
})
